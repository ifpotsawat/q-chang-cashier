package handlers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/repositories/product"
)

type Product struct {
}

type responseListProduct struct {
}

func (Product) List(e echo.Context) error {
	products := product.GetAllProducts()
	return e.JSON(http.StatusOK, products)
}
func (Product) GetInfo(e echo.Context) error {
	idParam := e.Param("id")
	if idParam == "" {
		return e.JSON(http.StatusUnprocessableEntity, "id is empty")
	}
	id, err := strconv.Atoi(idParam)
	if err != nil {
		return e.JSON(http.StatusInternalServerError, "parse id from param to int error")
	}
	pd := product.FindByID(id)
	if pd == nil {
		return e.JSON(http.StatusNoContent, "product not found")
	}
	return e.JSON(http.StatusOK, pd)

}
