package handlers

import (
	"math"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/repositories/cashier"
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/repositories/product"
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/schemas"
)

type Cashier struct {
}
type productRequest struct {
	ProductID int `json:"productID" form:"productID" validate:"required"`
	Quantity  int `json:"quantity" form:"quantity" validate:"required"`
}
type ChangeRequest struct {
	Paid       float64          `json:"paid" form:"paid" validate:"required"`
	ProductReq []productRequest `json:"productReq" form:"productReq" validate:"required"`
}
type ChangeResponse struct {
	Change    []schemas.Bank
	Available []schemas.Bank
}

func (Cashier) List(e echo.Context) error {
	allBanks := cashier.GetAllBanks()
	return e.JSON(http.StatusOK, allBanks)
}
func (Cashier) Change(e echo.Context) error {
	request := ChangeRequest{}
	if err := e.Bind(&request); err != nil { //bind request form error
		return e.JSON(http.StatusUnprocessableEntity, "bind form error")
	}
	if err := e.Validate(request); err != nil { //request field is invalid format or required
		return e.JSON(http.StatusUnprocessableEntity, "invalid form")
	}
	var productPrice float64
	for _, pdReq := range request.ProductReq {
		pd := product.FindByID(pdReq.ProductID)
		if pd == nil { //product not found
			return e.JSON(http.StatusInternalServerError, "invalid product id")
		}
		productPrice += (pd.Price * float64(pdReq.Quantity))
	}

	mapBankAmount, sortedBank := cashier.MapBankAmount()
	result := make(map[float64]int)
	res := []schemas.Bank{}
	if productPrice > request.Paid { //paid lower than product price
		return e.JSON(http.StatusForbidden, "insufficient banks or coins")
	}
	change := request.Paid - productPrice
	if change == 0 {
		return e.JSON(http.StatusNoContent, "no change")
	}
	for change > 0 {
		var toUse float64
		for _, bank := range sortedBank {

			if change >= bank && mapBankAmount[bank] != 0 {
				toUse = bank
				continue
			}

			count, remain := calChange(change, toUse, mapBankAmount)

			result[toUse] = int(count)
			bank := schemas.Bank{
				Bank:   toUse,
				Amount: int(count),
			}
			res = append(res, bank)
			change = remain
			break
		}
	}
	available := cashier.AvailableList(mapBankAmount)
	response := ChangeResponse{
		Change:    res,
		Available: available,
	}
	return e.JSON(http.StatusOK, response)
}
func calChange(currentChange float64, bank float64, bankAmount map[float64]int) (float64, float64) {
	available := bankAmount[bank]
	need := currentChange / bank
	if available < int(need) && available != 0 {
		use := math.Mod(float64(available), need)
		bankAmount[bank] = available - int(use)

		return use, currentChange - (bank * float64(use))
	}

	remain := math.Mod(currentChange, bank)
	bankAmount[bank] = available - int(need)
	return need, remain
}
