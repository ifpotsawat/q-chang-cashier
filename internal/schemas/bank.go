package schemas

type Bank struct {
	Bank   float64 `json:"bank"`
	Amount int     `json:"amount"`
}
