package product

import "gitlab.com/ifpotsawat/q-chang-cashier/internal/schemas"

//In this scenario I'll mock data in simple struct

func GetAllProducts() []schemas.Product {
	return []schemas.Product{
		{
			ID:    1,
			Name:  "Wood",
			Price: 2000,
		}, {
			ID:    2,
			Name:  "Steel",
			Price: 3000,
		}, {
			ID:    3,
			Name:  "Nail",
			Price: 100,
		},
	}
}

func FindByID(id int) *schemas.Product {
	products := GetAllProducts()
	for _, product := range products {
		if product.ID == id {
			return &product
		}
	}
	return nil
}
