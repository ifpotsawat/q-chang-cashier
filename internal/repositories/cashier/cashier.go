package cashier

import (
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/schemas"
)

//In this scenario I'll mock data in simple struct

//GetAllBanks return structs of banks
func GetAllBanks() []schemas.Bank {
	return []schemas.Bank{
		{
			Bank:   0.25,
			Amount: 50,
		},
		{
			Bank:   1,
			Amount: 20,
		},
		{
			Bank:   5,
			Amount: 20,
		},
		{
			Bank:   10,
			Amount: 20,
		},
		{
			Bank:   20,
			Amount: 30,
		},
		{
			Bank:   50,
			Amount: 20,
		},
		{
			Bank:   100,
			Amount: 20,
		},
		{
			Bank:   500,
			Amount: 20,
		},
		{
			Bank:   1000,
			Amount: 10,
		},
	}

}

//MapBankAmount func return map bank as key and amount as value
func MapBankAmount() (map[float64]int, []float64) {
	banks := GetAllBanks()
	ba := make(map[float64]int)
	sortedBanks := []float64{}
	for _, b := range banks {
		ba[b.Bank] = b.Amount
		sortedBanks = append(sortedBanks, b.Bank)
	}
	return ba, sortedBanks
}

//AvailableList func return non zero amount of banks or coins
func AvailableList(usedBank map[float64]int) []schemas.Bank {
	banks := GetAllBanks()
	for i, data := range banks {
		if v, ok := usedBank[data.Bank]; ok {
			banks[i].Amount = v
		}

	}

	return banks
}
