package routes

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/ifpotsawat/q-chang-cashier/internal/handlers"
)

func RouteAPI(e *echo.Echo) {
	api := e.Group("/api")

	cas := api.Group("/cashier")
	cas.GET("", handlers.Cashier{}.List)
	cas.POST("/change", handlers.Cashier{}.Change)

	prod := api.Group("/products")

	prod.GET("", handlers.Product{}.List)
	prod.GET("/:id", handlers.Product{}.GetInfo)
}
