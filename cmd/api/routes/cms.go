package routes

import "github.com/labstack/echo/v4"

func RouteCMS(e *echo.Echo) {
	/*
		initiate route for CRUD banks or coins

		ex.
		- Cashier
			Create - POST /cms/cashier/banks
				request
					body
						{
							"bank": 100,
							"amount": 5
						}

				response
				status 201
					body
						{
							"bank": 100,
							"amount": 5
						}
			Update - PUT /cms/cashier/banks/:id
				request
					parameter id = 1
					body
						{
							"bank": 100,
							"amount": 10
						}

				response
				status 200
					body
						{
							"bank": 100,
							"amount": 10
						}
			Delete - DELETE /cms/cashier/banks/:id
					request
						parameter id = 1

					response
					status 204

		- Product

		Create - POST /cms/products
				request
					body
						{
							"name": "Wood",
							"price": 2000
						}

				response
				status 201
					body
						{
							"name": "Wood",
							"price": 2000
						}
			Update - PUT /cms/products/:id
				request
					parameter id = 1
					body
						{
							"name": "Wood Tech",
							"price": 2500
						}

				response
				status 200
					body
						{
							"id": 1
							"name": "Wood Tech",
							"price": 2500
						}
			Delete - DELETE /cms/products/:id
					request
						parameter id = 1

					response
					status 204
	*/
}
