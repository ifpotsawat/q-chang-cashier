# Q-Chang Backend Developer Assignment

 This project repository consists of Designing an API from the assignment, simple Golang code that implements API for cashier system (ex. change API), Designing features & functions, and System flows.



## Usage
Kindly test an API. Please import collection from this URL to **Postman**.

[https://www.getpostman.com/collections/c45b7ad7dbdde3fd4d6c](https://www.getpostman.com/collections/c45b7ad7dbdde3fd4d6c)

On Q-Chang collection in Postman consist of the following API.
* cashier
  * List banks or coins
  * change
* Product
  * List products
  * Get Product Info


Then, Please run this command at root of this repository.
```bash
go run cmd/api/main.go
```
to start local server at localhost port 80 (default).

## Assignment
### 1. Design API to find value x,y,z in data set [1, X, 8, 17, Y, Z, 78, 113]

##### Case: x,y,z is all required fill

API: GET /api/someroute

Request body 

```json
{
 "value1": x,
 "value2": y,
 "value3": z,
}
```

Response body 
 
    ex .status 200 - found all x,y,z  



```json
[
 {
  "value1": x,
  "index": 1 
 },
 {
  "value2": y,
  "index": 4 
 },
 {
  "value3": z,
  "index": 5 
 }
]
```
ex. status 204 - **x** or **y** or **z** is not found

ex. status 422 - **x** or **y** or **z** is empty

### 2. Design API to support auto cashier system


###  feature to calculate the change money when the customer pays with the cash value larger than the product price

 I have implemented with Golang and testable by Postman as I mentioned the instruction above.
### features and functions that the system should have

 
- List Products - GET /api/products 

  Response
  
      ex. status 200 - return all products
```json
 [
    {
        "id": 1,
        "name": "Wood",
        "price": 2000
    },
    {
        "id": 2,
        "name": "Steel",
        "price": 3000
    },
    {
        "id": 3,
        "name": "Nail",
        "price": 100
    }
]
```

- Get Product Info - GET /api/products/:id

  request parameter: id = 1

  Response

      ex. status 200 - return product info of ID = 1
```json
 {
   "id": 1,
   "name": "Wood",
   "price": 2000
 }
```

        ex. status 204 - product ID 1 is not found
- Calculate Change - POST /api/cashier/change

   Request body 
```json
 {
    "paid": 2500,
    "productReq": [
        {
            "productID": 1,
            "quantity": 1
        },{
            "productID": 3,
            "quantity": 1
        }
    ]
}
```

    Response 
    ex. status 200 - return banks and amount of change, available banks or coins
```json
{
    "Change": [
        {
            "bank": 100,
            "amount": 1
        }
    ],
    "Available": [
        {
            "bank": 0.25,
            "amount": 50
        },
        {
            "bank": 1,
            "amount": 20
        },
        {
            "bank": 5,
            "amount": 20
        },
        {
            "bank": 10,
            "amount": 20
        },
        {
            "bank": 20,
            "amount": 30
        },
        {
            "bank": 50,
            "amount": 20
        },
        {
            "bank": 100,
            "amount": 19
        },
        {
            "bank": 500,
            "amount": 20
        },
        {
            "bank": 1000,
            "amount": 10
        }
    ]
}
```

      ex. status 422 - invalid input form
      ex. status 403 - insufficient banks or coins
**Others function**
- Change canceling
- Billing
- Product Management (CRUD)
- Bank Notes and Coins Management (CRUD)
 

PS. for some feature or function I have implemented on Golang code and testable on postman 


## System flows
Please click 
[Diagram URL](https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1&title=system%20flows#R7VrbkuI2EP0aqnYfdso3DDwCO7tJVZKaylRuT1vCFrZqZMmR5AHy9Wlh%2BSpPYFhgEjYvg9xqt8Q53a1uDSN%2FmW0%2FC5SnP%2FIY05HnxNuR%2F3HkeW7ghfChJbtSMpm4pSARJDZKjeCR%2FIWN0DHSgsRYdhQV51SRvCuMOGM4Uh0ZEoJvumprTrur5ijBluAxQtSW%2FkZilZbS6dhp5N9hkqTVyq5jZjJUKRuBTFHMNy2Rfz%2Fyl4JzVY6y7RJTDV6FS%2Fnepxdm640JzNQxL%2FzE2O%2FpEv%2BhEorjX93F4%2FP25w%2FGyjOihfnCSyRTggUIPwnOFGax2b7aVZgIXrAYa7POyF9sUqLwY44iPbsBLwBZqjIKTy4MzQJYKLx9cedujQc4EuYZVmIHKuYFv0J7V%2FmGed40jLgVzGmLjdDIkHGCpDbd4AQDA9UrYPMs2EZeSGHVhcwRg3Gixw2UCxQ97ZEslVaiUqkksIv2m28OeRj82yD3ByDvgQQRluthkdF5pLgAKDQIBKL5B7TC9IFLoghnoLLiSvGspTCnJNETivfA5IWihOFlnV%2Bc8yA8Owywf018g6Nc%2BhcJ%2FuzPQfE%2ByynfYXysBwNSqousVII%2FAbBUM%2FWRcQaaizWhtCdChpoIkMVigLOMxLFeZjAuupFzBuLG4w5xk4HAcAaI8y5F3PhwYEDqmeuzsME0htS0B8UdSh04tg7FHjpwCiORYHUoYG0UWziNB2CqZAJTpMhzdxtD2JkVHjiBDdYkuU6XpXEffskLEWHzVvvUtAwFd11T7rRnqkTCMgWIo11LLdcK8vgt%2B37nPIdBabHxlBrV051nOnkL73krrwhc5%2B5MfhG4vfzdT8wveMXZiJt%2BS8RNHOc8tIWzWT%2Bcr03c7FXERRRJSaL%2FFl2QNDsYW4ffqVFmGbowWVWR1iJLYiSiFGQPgsdFpDf7PVtz%2BHi3gBlogDWXEYJyUVf%2BXP8hLC%2FUvvsU2XuL7ZupjNw%2BWwM1bV33XqU2mtn97c0FmxUjwYnB1k%2BylqFLB5vdVX%2BG5cow0zH27r3dQAMNCBbexxrIUKY92zQk7ae8NPOFxLU86WjdeNcSeN1jr75ga8dmOL5mbNodvcCqEGwgt%2FZYadHRR7Vu7FthzXPM%2BsUPTJvLxulXxfiZC5u6nfzqk7Jv6NLBO3R%2FcGOZ1nW8Dsa%2BeypZzvSfDV2aLPvOQGKK9wGX16GH9IWlo1PbvoSBP38WiCmidvVkXna4GWQrdbu506prZm9e14S3H22B694N34icUNkcNHXpiLPvWZYpYgkeqmlWXP%2B3bP6KimbZCc5lGZlfqrj8NuudsE%2F5UDdy3YrHvrOpKh5DClvJfP%2F9nbnhzlkg9gQfjCss95tXuvmM9s4DgwzQ39U2NfV9b6JEajt8rb%2FSMyIUrah%2BcwWGZdWhRuD48obrrHA27btDGJx8eB8ydelkYl8hxUTmVB%2FG%2FzvScfnBuXPCcNKh8YN7mYbcn514bLnOAUMv%2BFljqFLk67XEr%2FRFeGx%2BnFCqNz%2Fx8O%2F%2FBg%3D%3D)