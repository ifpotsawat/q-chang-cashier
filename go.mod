module gitlab.com/ifpotsawat/q-chang-cashier

go 1.13

require (
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/go-playground/validator/v10 v10.4.1
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.1
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
